<?php

function id_entidad_invalido($cve_entidad) {
  if (strlen($cve_entidad) != 2) {
    return true;
  }
}

//GET
$app->get('/entidades',function() use($app){
  $datos = array('entidades'=>array());
  $entidades=$app->db->query("select * from entidad");
  if(!$entidades){
    $app->halt(500, "Error: No se puede acceder");
  }
  foreach($entidades as $entidades){
    $datos['entidades'][]=$entidades;
  }
  $app->response->headers->set('Content-Type','application/xml');
  $app->response->setStatus(200);
  $app->render('entidades.php',$datos);
});

//GET
$app->get('/municipios',function() use($app){
  $datos = array('municipios'=>array());
  $municipios=$app->db->query("select * from municipio");
  if(!$municipios){
    $app->halt(500, "Error: No se puede acceder");
  }
  foreach($municipios as $municipios){
    $datos['municipios'][]=$municipios;
  }
  $app->response->headers->set('Content-Type','application/xml');
  $app->response->setStatus(200);
  $app->render('municipios.php',$datos);
});

//GET
$app->get('/indicadores',function() use($app){
  $datos = array('indicadores'=>array());
  $indicadores=$app->db->query("select * from indicador");
  if(!$indicadores){
    $app->halt(500, "Error: No se puede acceder");
  }
  foreach($indicadores as $indicadores){
    $datos['indicadores'][]=$indicadores;
  }
  $app->response->headers->set('Content-Type','application/xml');
  $app->response->setStatus(200);
  $app->render('indicadores.php',$datos);
});

//GET
$app->get('/temas_nivel_1',function() use($app){
  $datos = array('tema_nivel_1'=>array());
  $tema_nivel_1=$app->db->query("select * from tema_nivel_1");
  if(!$tema_nivel_1){
    $app->halt(500, "Error: No se puede acceder");
  }
  foreach($tema_nivel_1 as $tema_nivel_1){
    $datos['tema_nivel_1'][]=$tema_nivel_1;
  }
  $app->response->headers->set('Content-Type','application/xml');
  $app->response->setStatus(200);
  $app->render('temas_nivel_1.php',$datos);
});

//GET
$app->get('/temas_nivel_2',function() use($app){
  $datos = array('tema_nivel_2'=>array());
  $tema_nivel_2=$app->db->query("select * from tema_nivel_2");
  if(!$tema_nivel_2){
    $app->halt(500, "Error: No se puede acceder");
  }
  foreach($tema_nivel_2 as $tema_nivel_2){
    $datos['tema_nivel_2'][]=$tema_nivel_2;
  }
  $app->response->headers->set('Content-Type','application/xml');
  $app->response->setStatus(200);
  $app->render('temas_nivel_2.php',$datos);
});

//GET
$app->get('/temas_nivel_3',function() use($app){
  $datos = array('tema_nivel_3'=>array());
  $tema_nivel_3=$app->db->query("select * from tema_nivel_3");
  if(!$tema_nivel_3){
    $app->halt(500, "Error: No se puede acceder");
  }
  foreach($tema_nivel_3 as $tema_nivel_3){
    $datos['tema_nivel_3'][]=$tema_nivel_3;
  }
  $app->response->headers->set('Content-Type','application/xml');
  $app->response->setStatus(200);
  $app->render('temas_nivel_3.php',$datos);
});


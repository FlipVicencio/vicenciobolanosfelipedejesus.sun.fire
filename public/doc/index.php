<!DOCTYPE html>
  <head>
    <meta charset="utf-8" />
    <title>Documentación de API</title>
    <link rel="stylesheet" type="text/css" href="../css/style.css" />
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link href="../css/bootstrap-responsive.css" rel="stylesheet">
  </head>
  <body>
    <div class="col-md-1">
    </div>
    <div class="col-md-10">
	  <a href="../">Regresar</a>
	  <form class="well">
	  <h1>Documentación de API</h1>
	  <p>
	    La API se basa en los principios de REST y expone los datos del INEGI respecto a la encuesta nacional de Ocupacion y Empleo 
	  </p>
	  <p>
	    La URL raiz es <a href="http://vicenciobolanosfelipedejesus.sun.fire/api/">http://vicenciobolanosfelipedejesus.sun.fire/api/</a>.
	  </p>
      </form>
      <form class="well">
	  <h2>Obtener Entidades</h2>
	  <p>Descripción: Obtiene las Entidades de la base de datos.</p>
	  <ul>
	    <li><strong>URL</strong>: http://vicenciobolanosfelipedejesus.sun.fire/api/entidades</li>
	    <li><strong>Método HTTP</strong>: GET</li>
	    <li><strong>Presentación de respuesta</strong>: XML</li>
	    <li><strong>Ejemplo de respuesta</strong>:
      <pre>
      &lt;xml version="1.0" encoding="UTF-8"&gt;
       &lt;catalog&gt;
	&lt;entidades id=""&gt;
		&lt;entidad&gt; &lt;"Entidad"&gt; &lt;entidad&gt;
       &lt;entidades&gt;
      &lt;catalog&gt;
      </pre>
	    </li>
	  </ul>
    <hr />

	  <h2>Obtener Municipios</h2>
	  <p>Descripción: Obtiene todos los municipios de la base de datos.</p>
	  <ul>
	    <li><strong>URL</strong>: http://vicenciobolanosfelipedejesus.sun.fire/api/municipios</li>
	    <li><strong>Método HTTP</strong>: GET</li>
	    <li><strong>Presentación de respuesta</strong>: XML</li>
	    <li><strong>Ejemplo de respuesta</strong>:
      <pre>
      &lt;xml version="1.0" encoding="UTF-8"&gt;
       &lt;catalog&gt;
	&lt;municipios id=""&gt;
		&lt;municipio&gt; &lt;"Municipio"&gt; &lt;municipio&gt;
       &lt;municipios&gt;
      &lt;catalog&gt;
      </pre>
	    </li>
	  </ul>
      
    <hr />

	  <h2>Obtener Indicadores</h2>
	  <p>Descripción: Obtiene todos los indicadores de la encuesta</p>
	  <ul>
	    <li><strong>URL</strong>: http://vicenciobolanosfelipedejesus.sun.fire/api/indicadores</li>
	    <li><strong>Método HTTP</strong>: GET</li>
	    <li><strong>Presentación de respuesta</strong>: XML</li>
	    <li><strong>Ejemplo de respuesta</strong>:
      <pre>
      &lt;xml version="1.0" encoding="UTF-8"&gt;
       &lt;catalog&gt;
	&lt;indicadores id=""&gt;
		&lt;indicador&gt; &lt;"Indicador"&gt; &lt;indicador&gt;
       &lt;indicadores&gt;
      &lt;catalog&gt;
      </pre>
	    </li>
	  </ul>
      <hr />

	  <h2>Obtener Temas de Nivel 1</h2>
	  <p>Descripción: Obtiene todos los Temas de Nivel 1</p>
	  <ul>
	    <li><strong>URL</strong>: http://vicenciobolanosfelipedejesus.sun.fire/api/tenas_nivel_1</li>
	    <li><strong>Método HTTP</strong>: GET</li>
	    <li><strong>Presentación de respuesta</strong>: XML</li>
	    <li><strong>Ejemplo de respuesta</strong>:
      <pre>
      &lt;xml version="1.0" encoding="UTF-8"&gt;
       &lt;catalog&gt;
	&lt;temas id=""&gt;
		&lt;tema1&gt; &lt;"Nombre de Tema nivel 1"&gt; &lt;tema1&gt;
       &lt;temas&gt;
      &lt;catalog&gt;
      </pre>
	    </li>
	  </ul>
          <hr />

	  <h2>Obtener Temas de Nivel 2</h2>
	  <p>Descripción: Obtiene todos los Temas de Nivel 2</p>
	  <ul>
	    <li><strong>URL</strong>: http://vicenciobolanosfelipedejesus.sun.fire/api/temas_nivel_2</li>
	    <li><strong>Método HTTP</strong>: GET</li>
	    <li><strong>Presentación de respuesta</strong>: XML</li>
	    <li><strong>Ejemplo de respuesta</strong>:
      <pre>
      &lt;xml version="1.0" encoding="UTF-8"&gt;
       &lt;catalog&gt;
	&lt;temas id=""&gt;
		&lt;tema2&gt; &lt;"Nombre de Tema nivel 2"&gt; &lt;tema2&gt;
       &lt;temas&gt;
      &lt;catalog&gt;
      </pre>
	    </li>
	  </ul>
	 
          <hr />

	  <h2>Obtener Temas de Nivel 3</h2>
	  <p>Descripción: Obtiene todos los Temas de Nivel 3</p>
	  <ul>
	    <li><strong>URL</strong>: http://vicenciobolanosfelipedejesus.sun.fire/api/temas_nivel_3</li>
	    <li><strong>Método HTTP</strong>: GET</li>
	    <li><strong>Presentación de respuesta</strong>: XML</li>
	    <li><strong>Ejemplo de respuesta</strong>:
      <pre>
      &lt;xml version="1.0" encoding="UTF-8"&gt;
       &lt;catalog&gt;
	&lt;temas id=""&gt;
		&lt;tema3&gt; &lt;"Nombre de Tema nivel 3"&gt; &lt;tema3&gt;
       &lt;temas&gt;
      &lt;catalog&gt;
      </pre>
	    </li>
	  </ul>
    </form>
      </div>
  </body>
</html>
